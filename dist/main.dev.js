"use strict";

$('.nav ul li').click(function () {
  $(this).addClass("active").siblings().removeClass('active');
});
var tabBtn = document.querySelectorAll('.nav ul li');
var tab = document.querySelectorAll('.tab');

function tabs(panelIndex) {
  tab.forEach(function (node) {
    node.style.display = 'none';
  });
  tab[panelIndex].style.display = 'block';
}

tabs(0);
var bio = document.querySelector('.bio');

function bioText() {
  bio.oldText = bio.innerText;
  bio.innerText = bio.innerText.substring(0, 150) + "...";
  bio.innerHTML += "&nbsp;" + "<span onclick='addLength()' id='see-more-bio'> See More</span>";
}

bioText();

function addLength() {
  bio.innerHTML = bio.oldText;
  bio.innerHTML += "&nbsp;" + "<span onclick='bioText()' id='see-less-bio'> See Less</span>";
}